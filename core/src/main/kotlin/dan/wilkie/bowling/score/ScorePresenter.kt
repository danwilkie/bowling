package dan.wilkie.bowling.score

import dan.wilkie.bowling.common.Presenter
import dan.wilkie.bowling.common.RxSchedulers
import io.reactivex.Observable
import io.reactivex.Observable.timer
import io.reactivex.Observable.just
import java.util.concurrent.TimeUnit.MILLISECONDS

class ScorePresenter(
        private val scoreCalculator: ScoreCalculator,
        private val schedulers: RxSchedulers) : Presenter<ScorePresenter.View>() {

    override fun onAttach(view: View) {
        super.onAttach(view)
        addDisposable(view.onGameEntered()
                .doOnNext { view.clear() }
                .switchMap { game ->
                    just(game)
                            .filter { game.isNotEmpty() }
                            .map { scoreCalculator.calculateScore(it) }
                            .subscribeOn(schedulers.computation)
                            .observeOn(schedulers.ui)
                            .onErrorResumeNext { _: Throwable ->
                                showErrorAfterDelay(view)
                                        .filter { false }
                            }
                }
                .subscribe { view.showScore(it) }
        )
    }

    private fun showErrorAfterDelay(view: View): Observable<Int> {
        return timer(500, MILLISECONDS, schedulers.computation)
                .observeOn(schedulers.ui)
                .doOnNext { view.showError() }
                .map { it.toInt() }
    }

    interface View : Presenter.View {
        fun onGameEntered(): Observable<String>
        fun showScore(score: Int)
        fun showError()
        fun clear()
    }
}