package dan.wilkie.bowling.score

interface ScoreCalculator {
    fun calculateScore(game: String): Int
}

class ScoreCalculatorImpl : ScoreCalculator {

    override fun calculateScore(game: String): Int {
        val gameAndBonus = game.split("||").validate { size == 2 }
        val mainGame = gameAndBonus[0]
        val bonus = gameAndBonus[1]
        val frames = mainGame.split('|').validate { size == 10 }.plus(bonus).plus("")

        return (0..9).sumBy {
            frameScore(frames[it], frames[it + 1], frames[it + 2])
        }
    }

    private fun frameScore(frame: String, nextFrame: String, frameAfterNext: String) = when {
        frame.isStrike() -> 10 + strikeBonus(nextFrame, frameAfterNext)
        frame.isSpare() -> 10 + nextFrame.firstBall()
        else -> frame.sumOfBothBalls().validate { this <= 10 }
    }

    private fun strikeBonus(nextFrame: String, frameAfterNext: String) = when {
        nextFrame.isDoubleStrike() -> 20
        nextFrame.isStrike() -> 10 + frameAfterNext.firstBall()
        nextFrame.isSpare() -> 10
        else -> nextFrame.sumOfBothBalls()
    }

    private fun String.isDoubleStrike() = this == "XX"
    private fun String.isStrike() = this == "X"
    private fun String.isSpare() = this.length == 2 && this[1] == '/'
    private fun String.firstBall() = this[0].score()
    private fun String.secondBall() = this[1].score()
    private fun String.sumOfBothBalls() = this.validate { length == 2 && isNotStrikeOnSecondBall() }.firstBall() + this.secondBall()
    private fun String.isNotStrikeOnSecondBall() = this[1] != 'X'

    private fun Char.score() = when (this) {
        '-' -> 0
        'X' -> 10
        else -> this.toString().toInt()
    }

    private fun <T> T.validate(predicate: T.() -> Boolean): T {
        if (!predicate(this)) throw IllegalArgumentException("Invalid game string")
        return this
    }
}
