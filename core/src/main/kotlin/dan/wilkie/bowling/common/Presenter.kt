package dan.wilkie.bowling.common

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Presenter<in V: Presenter.View> {
    private val disposables: CompositeDisposable = CompositeDisposable()
    private var attachedView: V? = null

    fun attach(view: V) {
        if (attachedView != null) throw IllegalStateException("View is already attached")
        attachedView = view
        onAttach(view)
    }

    fun detach(view: V) {
        if (attachedView != view) throw IllegalStateException("View is not already attached")
        attachedView = null
        disposables.clear()
        onDetach(view)
    }

    protected open fun onAttach(view: V) {}

    protected open fun onDetach(view: V) {}

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    interface View
}