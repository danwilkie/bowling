package dan.wilkie.bowling.common

import io.reactivex.Scheduler

data class RxSchedulers(val io: Scheduler, val computation: Scheduler, val ui: Scheduler)