package dan.wilkie.bowling.score

import dan.wilkie.bowling.score.ScoreCalculatorImpl
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class ScoreCalculatorValidIInputTest(private val game: String, private val expectedScore: Int) {

    companion object {
        @Parameterized.Parameters
        @JvmStatic
        fun data() = listOf(
                arrayOf("X|X|X|X|X|X|X|X|X|X||XX", 300),
                arrayOf("9-|9-|9-|9-|9-|9-|9-|9-|9-|9-||", 90),
                arrayOf("5/|5/|5/|5/|5/|5/|5/|5/|5/|5/||5", 150),
                arrayOf("X|7/|9-|X|-8|8/|-6|X|X|X||81", 167),
                arrayOf("X|X|X|X|X|X|X|X|X|2/||7", 269),
                arrayOf("44|53|62|71|-1|1-|--|X|3/|X||X3", 97),
                arrayOf("2-|2-|-2|-2|22|22|2/|2/|X|2/||X", 88),
                arrayOf("1/|1/|1/|1/|1/|1/|1/|1/|1/|1/||-", 109),
                arrayOf("X|X|X|X|X|X|X|X|X|X||36", 282),
                arrayOf("--|--|--|--|--|--|--|--|--|X||--", 10),
                arrayOf("1-|1-|1-|1-|1-|5/|1-|1-|1-|1-||", 20),
                arrayOf("--|--|--|--|--|--|--|--|--|--||", 0)
        )
    }

    @Test
    fun shouldCalculateScoreCorrectly() {
        val scoreCalculator = ScoreCalculatorImpl()

        val score = scoreCalculator.calculateScore(game)

        assertThat(score, equalTo(expectedScore))
    }
}