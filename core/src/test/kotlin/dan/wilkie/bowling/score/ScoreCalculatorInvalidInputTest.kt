package dan.wilkie.bowling.score

import org.junit.Test

class ScoreCalculatorInvalidInputTest {

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenGameEndIsMissing() {
        ScoreCalculatorImpl().calculateScore("X|X|X|X|X|X|X|X|X|X")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenTooFewFramesAreProvided() {
        ScoreCalculatorImpl().calculateScore("X|X|X|X|X|X|X||")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenTooManyFramesAreProvided() {
        ScoreCalculatorImpl().calculateScore("X|X|X|X|X|X|X|X|X|X|X|X|X||")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenFrameHasOnlyOneBall() {
        ScoreCalculatorImpl().calculateScore("3|X|X|X|X|X|X|X|X|X||")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenFrameHasThreeBalls() {
        ScoreCalculatorImpl().calculateScore("333|X|X|X|X|X|X|X|X|X||")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenFrameHasMoreThanTenPins() {
        ScoreCalculatorImpl().calculateScore("39|X|X|X|X|X|X|X|X|X||")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenFrameHasInvalidCharacters() {
        ScoreCalculatorImpl().calculateScore("aa|X|X|X|X|X|X|X|X|X||")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenFrameIsEmpty() {
        ScoreCalculatorImpl().calculateScore("|X|X|X|X|X|X|X|X|X||")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenInputIsTextOnly() {
        ScoreCalculatorImpl().calculateScore("abcdef")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenInputIsNumericOnly() {
        ScoreCalculatorImpl().calculateScore("1234512345")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenBonusHasOneBallAfterStrike() {
        ScoreCalculatorImpl().calculateScore("X|X|X|X|X|X|X|X|X|X||4")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenBonusHasThreeBalls() {
        ScoreCalculatorImpl().calculateScore("X|X|X|X|X|X|X|X|X|X||444")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenBonusHasLessThanTenFollowedByStrike() {
        ScoreCalculatorImpl().calculateScore("X|X|X|X|X|X|X|X|X|X||4X")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenFrameHasLessThanTenFollowedByStrike() {
        ScoreCalculatorImpl().calculateScore("4X|X|X|X|X|X|X|X|X|X||XX")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenBonusHasTwoSections() {
        ScoreCalculatorImpl().calculateScore("4X|X|X|X|X|X|X|X|X|X||3|3")
    }

    @Test(expected = IllegalArgumentException::class)
    fun shouldRejectWhenFrameHasDoubleStrike() {
        ScoreCalculatorImpl().calculateScore("X|X|X|X|X|X|XX|X|X|X||XX")
    }
}
