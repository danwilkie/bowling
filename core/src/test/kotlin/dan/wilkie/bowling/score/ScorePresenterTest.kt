package dan.wilkie.bowling.score

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import dan.wilkie.bowling.common.RxSchedulers
import io.reactivex.schedulers.Schedulers.trampoline
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.mock

class ScorePresenterTest {
    private val scoreCalculator = mock(ScoreCalculator::class.java)
    private val view = mock(ScorePresenter.View::class.java)
    private val presenter = ScorePresenter(
            scoreCalculator,
            RxSchedulers(trampoline(), trampoline(), trampoline()))

    private val gameSubject = PublishSubject.create<String>()

    @Before
    fun setUp() {
        whenever(view.onGameEntered()).thenReturn(gameSubject)
    }

    @Test
    fun shouldShowScoreWhenValidGameIsEntered() {
        whenever(scoreCalculator.calculateScore(anyString())).thenReturn(300)
        presenter.attach(view)

        gameSubject.onNext("game")

        verify(view).showScore(300)
    }

    @Test
    fun shouldShowErrorWhenInvalidGameIsEntered() {
        whenever(scoreCalculator.calculateScore(anyString())).thenThrow(IllegalArgumentException())
        presenter.attach(view)

        gameSubject.onNext("game")

        verify(view).showError()
    }
}