package dan.wilkie.bowling.score

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.replaceText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import dan.wilkie.bowling.R
import org.junit.Rule
import org.junit.Test

class ScoreActivityTest {

    @Rule
    @JvmField
    val activityTestRule = ActivityTestRule<ScoreActivity>(ScoreActivity::class.java)

    @Test
    fun showsScoreWhenInputIsValid() {
        onView(withId(R.id.gameEditText)).perform(replaceText("X|X|X|X|X|X|X|X|X|X||XX"))
        onView(withId(R.id.scoreValue)).check(matches(withText("300")))
    }

    @Test
    fun showsErrorWhenInputIsInvalid() {
        onView(withId(R.id.gameEditText)).perform(replaceText("X|X|XXX"))
        onView(withId(R.id.scoreValue)).check(matches(withText("")))
        Thread.sleep(500)
        onView(withId(R.id.gameInputLayout)).check(matches(hasDescendant(withText(R.string.invalid_game))))
    }
}