package dan.wilkie.bowling.score

import dagger.Module
import dagger.Provides
import dan.wilkie.bowling.app.ActivityScope
import dan.wilkie.bowling.common.RxSchedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class ScoreModule {
    @Provides
    fun provideScoreCalculator() : ScoreCalculator = ScoreCalculatorImpl()

    @Provides
    fun provideRxSchedulers() = RxSchedulers(io = Schedulers.io(), computation = Schedulers.computation(), ui = AndroidSchedulers.mainThread())

    @Provides
    fun provideScorePresenter(scoreCalculator: ScoreCalculator, schedulers: RxSchedulers) = ScorePresenter(scoreCalculator, schedulers)
}