package dan.wilkie.bowling.score

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import dan.wilkie.bowling.R
import dan.wilkie.bowling.common.textChangesAsString
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_score.*
import javax.inject.Inject

class ScoreActivity : DaggerAppCompatActivity(), ScorePresenter.View {

    @Inject
    lateinit var presenter: ScorePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)
        presenter.attach(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detach(this)
    }

    override fun onGameEntered(): Observable<String> = gameEditText.textChangesAsString()

    override fun showScore(score: Int) {
        scoreValue.text = score.toString()
    }

    override fun showError() {
        gameInputLayout.error = getString(R.string.invalid_game)
    }

    override fun clear() {
        gameInputLayout.error = null
        scoreValue.text = ""
    }
}
