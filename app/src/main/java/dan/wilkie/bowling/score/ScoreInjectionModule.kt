package dan.wilkie.bowling.score

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dan.wilkie.bowling.app.ActivityScope

@Module
abstract class ScoreInjectionModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [ScoreModule::class])
    abstract fun provideScoreActivity(): ScoreActivity
}