package dan.wilkie.bowling.common

import android.widget.TextView
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable

fun TextView.textChangesAsString(): Observable<String> = textChanges().map { it.toString() }